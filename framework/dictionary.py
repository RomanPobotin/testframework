#!/usr/bin/env python
# -*- coding: utf-8 -*-
from framework.locators import *

DICTIONARY = {
    'admin_login':'login',
    'admin_pass':'pass'
}

PAGES = {
    'gmail': GoogleSearchLocators.gmail_icon,
    'help': GoogleSearchLocators.help_link
}
