#!/usr/bin/env bash

source virtualenv/bin/activate
pip3 install -r requirements.txt
deactivate